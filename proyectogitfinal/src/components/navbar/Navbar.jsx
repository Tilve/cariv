import { Link } from 'react-router-dom';
export function Navbar() {
  return (
    <nav className=' navbar navbar-expand-lg navbar-dark bg-primary fs-3'>
      <div className='container-fluid'>
        <Link className='navbar-brand' to='/'>
          CAR IV
        </Link>
        <button
          className='navbar-toggler'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon'></span>
        </button>

        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav me-auto mb-10 mb-lg-0'>
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/'>
                Home
              </Link>
            </li>

            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/Comandos'>
                Comandos
              </Link>
            </li>

            <li className='nav-item dropdown'>
              <Link
                className='nav-link dropdown-toggle'
                to='#'
                id='navbarDropdown'
                role='button'
                data-bs-toggle='dropdown'
                aria-expanded='false'>
                Equipo
              </Link>

              <ul className='dropdown-menu' aria-labelledby='navbarDropdown'>
                <li>
                  <Link className='dropdown-item' to='/LideresProyecto'>
                    Lideres de Proyecto
                  </Link>
                </li>
                <li>
                  <Link className='dropdown-item' to='/DeveloperTeam'>
                    DT
                  </Link>
                </li>
              </ul>
            </li>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/Agradecimientos'>
                Agradecimientos
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
